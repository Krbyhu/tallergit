Nombre: Camilo Alvi�a Baos
RUT: 18.584.961-9

1.- Control de versiones distribuido.

El control de versiones es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones espec�ficas m�s adelante.

2.- git pull

Descarga el historial remoto y lo aplica al historial local.

3.- git push

Se envia el historial local para agregarlo al historial remoto.

4.- git clone

Obtiene una copia del repositorio Git existente.

5.- git checkout

Sirve para saltar de una rama a otra.
